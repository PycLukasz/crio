package com.example.crio.service;

import com.example.crio.controller.exception.BadRequestException;
import com.example.crio.controller.exception.NotFoundException;
import com.example.crio.model.Patient;
import com.example.crio.model.Visit;
import com.example.crio.repository.VisitRepository;
import com.example.crio.service.dto.CreateVisitDTO;
import com.example.crio.service.dto.ReturnVisitDTO;
import com.example.crio.service.mapper.ReturnVisitDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.*;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class VisitService {
    @Autowired
    private VisitRepository visitRepository;
    @Autowired
    private ReturnVisitDTOMapper mapper;
    @Autowired
    private PatientService patientService;

    public List<ReturnVisitDTO> visitsOfDay(LocalDate date) throws NotFoundException {
        List<Visit> visits =  visitRepository.findAll()
                        .stream().filter(v-> v.getLocalDate().equals(date))
                        .collect(Collectors.toList());
        return visits.stream()
                .map(v-> mapper.toDTO(v))
                .collect(Collectors.toList());
    }

    public ReturnVisitDTO createNewVisit(CreateVisitDTO newVisit, String login) throws BadRequestException {
        LocalDate localDate = LocalDate.parse(newVisit.getDate());
        LocalTime localTime = LocalTime.parse(newVisit.getHour() + ":" + newVisit.getMinute());
        LocalDateTime date = LocalDateTime.of(localDate, localTime);

        Patient patient = patientService.getUser(login);
        if (patientService.checkIfPatientIsActive(patient, localDate)){
            List<Visit> visits = visitRepository.findAll().stream()
                    .filter(v -> v.getLocalDate().equals(date.toLocalDate()))
                    .filter(v -> v.getLocalTime().equals(date.toLocalTime()))
                    .collect(Collectors.toList());
            if (visits.size() < 6){
                Visit visit = new Visit(null, date, patient);
                Visit saved = visitRepository.save(visit);
                patientService.timeToBreake(patient);
                return mapper.toDTO(saved);
            }

        }
        throw new BadRequestException();
    }


    public ReturnVisitDTO deleteVisit(int id) throws BadRequestException {
        Visit visitToDelete = visitRepository.findById(id).orElseThrow(() -> new BadRequestException());

        visitRepository.delete(visitToDelete);
        return mapper.toDTO(visitToDelete);
    }


}

