package com.example.crio.service;

import com.example.crio.controller.exception.BadRequestException;
import com.example.crio.model.Patient;
import com.example.crio.model.Visit;
import com.example.crio.service.dto.*;
import com.example.crio.service.mapper.FindPatientHistoryDTOMapper;
import com.example.crio.service.mapper.PatientDTOMapper;
import com.example.crio.repository.PatientRepository;
import com.example.crio.service.mapper.PatientTableDTOMapper;
import com.example.crio.service.mapper.ReturnVisitDTOMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientService {
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private PatientDTOMapper patientDTOMapper;
    @Autowired
    private PatientTableDTOMapper patientTableDTOMapper;
    @Autowired
    private ReturnVisitDTOMapper returnVisitDTOMapper;
    @Autowired
    private FindPatientHistoryDTOMapper findPatientHistoryDTOMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public CreatePatientDTO createNewPatient(CreatePatientDTO newPatient) {


        Patient patient = Patient.builder()
                .name(newPatient.getName())
                .surname(newPatient.getSurname())
                .login(newPatient.getLogin())
                .password(passwordEncoder.encode(newPatient.getPassword()))
                .isAdmin(false)
                .pesel(newPatient.getPesel())
                .street(newPatient.getStreet())
                .postalCode(newPatient.getPostalCode())
                .city(newPatient.getCity())
                .email(newPatient.getEmail())
                .phoneNumber(newPatient.getPhoneNumber())
                .age(newPatient.getAge())
                .type(newPatient.getType())
                .discipline(newPatient.getDiscipline())
                .build();
        Patient addedPatient = patientRepository.save(patient);
        return patientDTOMapper.toDTO(addedPatient);
    }

    public List<PatientTableDTO> getAllPatients() {
        return patientRepository.findAll().stream()
                .map(c -> patientTableDTOMapper.toDTO(c))
                .collect(Collectors.toList());
    }


    public CreatePatientDTO updatePatientData(UpdatePatientDTO newData, String login) throws BadRequestException {
        Patient patientToUpdate = getUser(login);
        if(!newData.getName().isEmpty()){
            patientToUpdate.setName(newData.getName());
        }
        if(!newData.getSurname().isEmpty()){
            patientToUpdate.setSurname(newData.getSurname());
        }
        if(!newData.getPassword().isEmpty()){
            patientToUpdate.setPassword((BCrypt.hashpw(newData.getPassword(), BCrypt.gensalt(11))));
        }
        if(!newData.getStreet().isEmpty()){
            patientToUpdate.setStreet(newData.getStreet());
        }
        if(!newData.getPostalCode().isEmpty()){
            patientToUpdate.setPostalCode(newData.getPostalCode());
        }
        if(!newData.getCity().isEmpty()){
            patientToUpdate.setCity(newData.getCity());
        }
        if(!newData.getEmail().isEmpty()){
            patientToUpdate.setEmail(newData.getEmail());
        }
        if(!newData.getPhoneNumber().isEmpty()){
            patientToUpdate.setPhoneNumber(newData.getPhoneNumber());
        }
        if(!newData.getType().isEmpty()){
            patientToUpdate.setType(newData.getType());
        }
        if(!newData.getDiscipline().isEmpty()){
            patientToUpdate.setDiscipline(newData.getDiscipline());
        }
        Patient updatedPatient = patientRepository.save(patientToUpdate);
        return patientDTOMapper.toDTO(updatedPatient);
        }

    public PatientVisitDTO getMyHistory(Patient patient) throws BadRequestException {
        return getVisitsForPatient(findPatientHistoryDTOMapper.toDTO(patient));
    }
    public PatientVisitDTO getVisitsForPatient(FindPatientHistoryDTO findPatientHistoryDTO) throws BadRequestException {
        if(patientRepository.existsByPesel(findPatientHistoryDTO.getPesel())) {
            Patient patient = patientRepository.findByPesel(findPatientHistoryDTO.getPesel());
            checkIfPatientIsActive(patient, LocalDate.now());
            PatientVisitDTO patientVisitDTO = PatientVisitDTO.builder()
                    .data(patientTableDTOMapper.toDTO(patient))
                    .visits(patient.getVisits().stream().map(v-> returnVisitDTOMapper.toDTO(v))
                    .collect(Collectors.toList()))
                    .build();
            return patientVisitDTO;
        }
        throw new BadRequestException();
    }

    public boolean checkIfPatientIsActive(Patient patient, LocalDate date) {
        if (patient.getPause() != null) {
            if (patient.getPause().isBefore(LocalDate.now()) || patient.getPause().isEqual(LocalDate.now())) {
                patient.setPause(null);
                patientRepository.save(patient);
                return true;
            } else if (date.isAfter(patient.getPause())){
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }


    }

    public boolean timeToBreake(Patient patient) {
        if (patient.getVisits().size() % 15 == 14) {
            Visit lastVisit = patient.getVisits().get(patient.getVisits().size() - 1);
            patient.setPause(lastVisit.getLocalDate().plusMonths(2));
            patientRepository.save(patient);
            return  true;
        }
     return false;
    }


    public List<PatientTableDTO> findPatients(FindPatientDTO find) {
        if (!find.getDiscipline().isEmpty()){
            return patientRepository.findAllByDiscipline(find.getDiscipline()).stream()
                    .map(p -> patientTableDTOMapper.toDTO(p))
                    .collect(Collectors.toList());
        } else if (!find.getType().isEmpty()){
            return patientRepository.findAllByType(find.getType()).stream()
                    .map(p -> patientTableDTOMapper.toDTO(p))
                    .collect(Collectors.toList());
        } else if(!find.getPause().isEmpty()){
            List<Patient> all = new ArrayList<>();
            for (Patient p : patientRepository.findAll()) {
                if (p.getPause() != null) {
                    all.add(p);
                }
            }
            return all.stream().map(p -> patientTableDTOMapper.toDTO(p))
                    .collect(Collectors.toList());
        }
        return patientRepository.findAll().stream()
                .map(p -> patientTableDTOMapper.toDTO(p))
                .collect(Collectors.toList());

    }

    public Patient getUser(String login) {
        return patientRepository.findAll().stream().filter(u -> u.getLogin().equals(login)).findFirst().orElse(null);
    }


}

