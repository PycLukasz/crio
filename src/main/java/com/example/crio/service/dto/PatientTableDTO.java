package com.example.crio.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PatientTableDTO {
    private String name;
    private String surname;
    private String pesel;
    private String street;
    private String postalCode;
    private String city;
    private String email;
    private String phoneNumber;
    private Integer age;
    private String type;
    private String discipline;
    private LocalDate pause;

}
