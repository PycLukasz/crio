package com.example.crio.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreatePatientDTO {
    @Size(min=3)
    private String name;
    @Size(min=3)
    private String surname;
    private boolean isAdmin = false;
    @Size(min=3)
    private String login;
    @Size(min=3)
    private String password;
    @Size(min=11, max = 11)
    private String pesel;
    @Size(min=3)
    private String street;
    @Size(min=6, max = 6)
    private String postalCode;
    @Size(min=3)
    private String city;
    @Email
    @NotBlank
    private String email;
    @Size(min=9)
    private String phoneNumber;
    @Min(5)
    @NotNull
    private Integer age;
    private String type;
    private String discipline;

}
