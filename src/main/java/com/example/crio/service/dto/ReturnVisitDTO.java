package com.example.crio.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReturnVisitDTO {
    private Integer id;
    private String date;
    private String time;
    private String name;
    private String surname;
    private String login;

}
