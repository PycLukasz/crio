package com.example.crio.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;


@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UpdatePatientDTO {
    @Size(min=3)
    private String name;
    @Size(min=3)
    private String surname;
    @Size(min=3)
    private String password;
    @Size(min=3)
    private String street;
    @Size(min=6, max = 6)
    private String postalCode;
    @Size(min=3)
    private String city;
    @Email
    private String email;
    @Size(min=9)
    private String phoneNumber;
    @Min(5)
    private Integer age;
    private String type;
    private String discipline;

}
