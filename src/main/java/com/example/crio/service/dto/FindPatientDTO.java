package com.example.crio.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;




@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class FindPatientDTO {
    private String type;
    private String discipline;
    private String pause;

}
