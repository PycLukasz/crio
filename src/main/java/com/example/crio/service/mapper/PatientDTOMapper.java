package com.example.crio.service.mapper;

import com.example.crio.service.dto.CreatePatientDTO;
import com.example.crio.model.Patient;
import org.springframework.stereotype.Component;

@Component
public class PatientDTOMapper {
    public CreatePatientDTO toDTO(Patient addedPatient) {
        return CreatePatientDTO.builder()
                .name(addedPatient.getName())
                .surname(addedPatient.getSurname())
                .pesel(addedPatient.getPesel())
                .street(addedPatient.getStreet())
                .postalCode(addedPatient.getPostalCode())
                .city(addedPatient.getCity())
                .email(addedPatient.getEmail())
                .phoneNumber(addedPatient.getPhoneNumber())
                .age(addedPatient.getAge())
                .type(addedPatient.getType())
                .discipline(addedPatient.getDiscipline())
                .build();
    }
}
