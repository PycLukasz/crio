package com.example.crio.service.mapper;

import com.example.crio.model.Visit;
import com.example.crio.service.dto.ReturnVisitDTO;

import org.springframework.stereotype.Component;


@Component
public class ReturnVisitDTOMapper {
    public ReturnVisitDTO toDTO(Visit visit){
        return ReturnVisitDTO.builder()
                .id(visit.getVisitId())
                .date(visit.getLocalDate().toString())
                .time(visit.getLocalTime().toString())
                .name(visit.getPatient().getName())
                .surname(visit.getPatient().getSurname())
                .login((visit.getPatient().getLogin()))
                .build();
    }
}
