package com.example.crio.service.mapper;

import com.example.crio.model.Patient;
import com.example.crio.service.dto.FindPatientHistoryDTO;
import org.springframework.stereotype.Component;

@Component
public class FindPatientHistoryDTOMapper {
    public FindPatientHistoryDTO toDTO(Patient patient) {
        return FindPatientHistoryDTO.builder()
                .pesel(patient.getPesel())
                .build();
    }
}
