package com.example.crio.service.mapper;

import com.example.crio.model.Patient;
import com.example.crio.service.dto.PatientTableDTO;
import org.springframework.stereotype.Component;

@Component
public class PatientTableDTOMapper {
    public PatientTableDTO toDTO(Patient patient) {
        return PatientTableDTO.builder()
                .name(patient.getName())
                .surname(patient.getSurname())
                .pesel(patient.getPesel())
                .street(patient.getStreet())
                .postalCode(patient.getPostalCode())
                .city(patient.getCity())
                .email(patient.getEmail())
                .phoneNumber(patient.getPhoneNumber())
                .age(patient.getAge())
                .type(patient.getType())
                .discipline(patient.getDiscipline())
                .pause(patient.getPause())
                .build();
    }
}
