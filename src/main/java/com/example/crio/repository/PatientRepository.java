package com.example.crio.repository;

import com.example.crio.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface PatientRepository extends JpaRepository<Patient, Integer> {
    Patient findByNameAndSurname(String name, String surname);

    Patient findByPesel(String pesel);

    List<Patient> findAllByDiscipline(String discipline);

    List<Patient> findAllByType(String type);

    boolean existsByPesel(String pesel);
}
