package com.example.crio.repository;

import com.example.crio.model.Patient;
import com.example.crio.model.Visit;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.OffsetDateTime;
import java.util.List;


public interface VisitRepository extends JpaRepository<Visit, Integer> {
    Visit findVisitByDateAndPatient(OffsetDateTime date, Patient patient);
    List<Visit> findAllByPatient(Patient patient);
}
