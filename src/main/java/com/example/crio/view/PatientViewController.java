package com.example.crio.view;

import com.example.crio.controller.exception.BadRequestException;
import com.example.crio.model.Patient;
import com.example.crio.service.PatientService;
import com.example.crio.service.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import javax.validation.Valid;
import java.util.Collection;

@Controller
public class PatientViewController {
    @Autowired
    private PatientService patientService;

    @GetMapping("/all-patients")
    public ModelAndView allPatients(){
        ModelAndView mav = new ModelAndView("patients-table");
        mav.addObject("patients", patientService.getAllPatients());
        mav.addObject("find", new FindPatientDTO());
        return mav;
    }
    @PostMapping("/all-patients")
    public ModelAndView findPatients(@ModelAttribute FindPatientDTO find){
        ModelAndView mav = new ModelAndView("patients-table");
        mav.addObject("patients", patientService.findPatients(find));
        mav.addObject("find", new FindPatientDTO());
        return mav;
    }


    @GetMapping("/patientForm")
    public ModelAndView patientFormHandler(){
        ModelAndView mav = new ModelAndView("new-patient-form");
        mav.addObject("newPatient", new CreatePatientDTO());
        return mav;
    }
    @PostMapping("/patientForm")
    public String formHandler(@Valid @ModelAttribute(name = "newPatient") CreatePatientDTO newPatient, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "new-patient-form";
        }
        patientService.createNewPatient(newPatient);
        return "redirect:/home";
    }

    @GetMapping("/updateForm")
    public ModelAndView updatePatient() {
        ModelAndView mav = new ModelAndView("updateForm");
        UpdatePatientDTO patient = new UpdatePatientDTO();
        mav.addObject("patient", patient);
        return mav;
    }

    @PostMapping("/updateForm")
    public String updatePatientHandler(@Valid @ModelAttribute(name = "patient") UpdatePatientDTO newData, BindingResult bindingResult, Authentication authentication) throws BadRequestException {
        if (bindingResult.hasErrors()){
            return "updateForm";
        }
        patientService.updatePatientData(newData, authentication.getName());
        return "redirect:/home";
    }
    @GetMapping("/patientHistory")
    public String myHistory(Authentication authentication, Model model) throws BadRequestException {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        Patient patient = patientService.getUser(authentication.getName());
        PatientVisitDTO patientsVisit = patientService.getMyHistory(patient);
        model.addAttribute("patient", patientsVisit);
        return "patientHistory";
    }

    @GetMapping("patientsHistory")
    public ModelAndView patientHistoryForm(Authentication authentication){
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        ModelAndView mav = new ModelAndView("patientHistory-form");
        mav.addObject("patient", new FindPatientHistoryDTO());
        return mav;
    }
    @PostMapping ("/patientHistory")
    public String patientHistory(@Valid @ModelAttribute(name = "patient") FindPatientHistoryDTO patient, BindingResult bindingResult, Model model) throws BadRequestException {
        if (bindingResult.hasErrors()){
            return "patientHistory-form";
        }
        PatientVisitDTO patientsVisit = patientService.getVisitsForPatient(patient);
        model.addAttribute("patient", patientsVisit);
        return "patientHistory";

    }




}
