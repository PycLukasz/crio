package com.example.crio.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ViewController {
    @GetMapping("")
    public String goToTimetable(){
        return "redirect:/timetable";
    }

    @GetMapping("/")
    public String goToTimetable2(){
        return "redirect:/timetable";
    }


}
