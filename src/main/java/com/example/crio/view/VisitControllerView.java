package com.example.crio.view;

import com.example.crio.controller.exception.BadRequestException;
import com.example.crio.controller.exception.NotFoundException;
import com.example.crio.model.Day;
import com.example.crio.service.VisitService;
import com.example.crio.service.dto.CreateVisitDTO;
import com.example.crio.service.dto.ReturnVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.time.*;
import java.util.Collection;
import java.util.List;

@Controller
public class VisitControllerView {
    @Autowired
    private VisitService visitService;

    @GetMapping("/timetable")
    public ModelAndView timetable(Authentication authentication) throws NotFoundException {
        ModelAndView mav = new ModelAndView("timetable");
        mav.addObject("day", LocalDate.now());
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        mav.addObject("dayOfWeek", dayOfWeek.toString());
        List<ReturnVisitDTO> visitDTOS = visitService.visitsOfDay(LocalDate.now());
        mav.addObject("visits", visitDTOS);
        Day day = new Day();
        day.timetable();
        mav.addObject("hours", day.getDayTable());
        mav.addObject("getDate", new CreateVisitDTO());
        mav.addObject("login", authentication.getName());
        return mav;
    }

    @PostMapping("/timetable")
    public ModelAndView timetableForDate(@ModelAttribute CreateVisitDTO dateFromForm) throws NotFoundException {
        ModelAndView mav = new ModelAndView("timetable");
        LocalDate date = LocalDate.parse(dateFromForm.getDate());
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        List<ReturnVisitDTO> visitDTOS = visitService.visitsOfDay(date);
        mav.addObject("day", date.toString());
        mav.addObject("dayOfWeek", dayOfWeek.toString());
        mav.addObject("visits", visitDTOS);
        Day day = new Day();
        day.timetable();
        mav.addObject("hours", day.getDayTable());
        mav.addObject("getDate", new CreateVisitDTO());
        return mav;
    }


    @GetMapping("/createVisit")
    public ModelAndView createVisitForm(){
        ModelAndView mav = new ModelAndView("visitForm");
        mav.addObject("newVisit", new CreateVisitDTO());
        return mav;
    }
    @PostMapping("/createVisit")
    public String createVisitHandler(@Valid @ModelAttribute (name = "newVisit") CreateVisitDTO newVisit, BindingResult bindingResult, Authentication authentication) throws BadRequestException {
        if (bindingResult.hasErrors()){
            return "visitForm";
        }
        visitService.createNewVisit(newVisit, authentication.getName());
        return "redirect:/home";
    }


    @GetMapping("/home")
    public String goHome(Authentication authentication){
        return "home";
    }



}
