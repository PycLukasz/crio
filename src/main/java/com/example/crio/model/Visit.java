package com.example.crio.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Visit {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer visitId;
    private LocalDateTime date;
    @ManyToOne
    private Patient patient;

    public LocalDate getLocalDate(){
        return date.toLocalDate();
    }
    public LocalTime getLocalTime(){
        return date.toLocalTime();
    }
}
