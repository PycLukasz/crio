package com.example.crio.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Integer id;
    private String login;
    private String password;
    private boolean isAdmin = false;
    private String name;
    private String surname;
    private String pesel;
    private String street;
    private String postalCode;
    private String city;
    private String email;
    private String phoneNumber;
    private Integer age;
    private String type;
    private String discipline;
    @OrderBy(value = "date")
    @OneToMany(mappedBy = "patient")
    private List<Visit> visits = new ArrayList<>();
    private LocalDate pause;


}
