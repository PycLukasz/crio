package com.example.crio.model;

import lombok.Getter;

public enum Discipline {
    NONE("nie dotyczy"),
    SPEEDWAY("żużel"),
    BASKETBALL("koszykówka"),
    VOLLEYBALL("siatkówka"),
    HOCKEY("hokej");

    @Getter
    private String displayName;
    Discipline(String displayName){
        this.displayName = displayName;
    }
}
