package com.example.crio.model;


import lombok.Getter;

public enum Type {
    NFZ("NFZ"),
    PRIVATE("prywatny"),
    SPORT("sportowiec");

    @Getter
    private String displayName;
    Type(String displayName){
        this.displayName = displayName;
    }
}
