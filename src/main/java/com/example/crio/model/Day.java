package com.example.crio.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Day {
    private List<String> dayTable = new ArrayList<>();

    public void timetable(){
        LocalTime time = LocalTime.parse("08:55:00");
        LocalTime finish = LocalTime.parse("12:00:00");
        while (time.isBefore(finish)) {
            time = time.plusMinutes(5);
            String[] split = time.toString().split("");
            int x = split.length;
            dayTable.add(time.toString());
        }
        time = LocalTime.parse("15:55:00");
        finish = LocalTime.parse("19:00:00");
        while (time.isBefore(finish)) {
            time = time.plusMinutes(5);
            dayTable.add(time.toString());
        }
    }

}
