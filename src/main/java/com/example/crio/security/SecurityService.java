package com.example.crio.security;

import com.example.crio.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecurityService {

    @Autowired
    private SecurityRepository securityRepository;

    public List<Patient> getUsers() {
        return securityRepository.findAll();
    }

    public Patient getUser(String username) {
        return getUsers().stream().filter(u -> u.getLogin().equals(username)).findFirst().orElse(null);
    }
}
