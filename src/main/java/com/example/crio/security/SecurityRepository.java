package com.example.crio.security;

import com.example.crio.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SecurityRepository extends JpaRepository<Patient, Integer> {

}
