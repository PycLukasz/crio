package com.example.crio.security;

import com.example.crio.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class SecurityDetailsServiceAdapter implements UserDetailsService {

    @Autowired
    private SecurityService securityService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        System.out.println("UserDetailsServiceAdapter called for username " + login);

        Patient user = securityService.getUser(login);
        if (user == null) {
            throw new UsernameNotFoundException("User with login " + login + " does not exist!");
        } else {
            String userRole = user.isAdmin() ? "ADMIN" : "USER";
            String passwordB = BCrypt.hashpw(user.getPassword(), BCrypt.gensalt(11));
            return org.springframework.security.core.userdetails.User
                    .withUsername(user.getLogin())
                    .password(user.getPassword())
                    .authorities("ROLE_" + userRole)
                    .build();
        }

    }
}
