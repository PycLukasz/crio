package com.example.crio.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityDetailsAdapterConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    public SecurityDetailsAdapterConfig(SecurityDetailsServiceAdapter securityDetailsServiceAdapter) {
        this.securityDetailsServiceAdapter = securityDetailsServiceAdapter;
    }
    private final SecurityDetailsServiceAdapter securityDetailsServiceAdapter;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(securityDetailsServiceAdapter).passwordEncoder(this.passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/home", "/patientForm", "/login").permitAll()
                .antMatchers("/css/**.css").permitAll()
                .antMatchers("/images/**.jpg").permitAll()
                .antMatchers("/all-patients").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .formLogin().defaultSuccessUrl("/home")
                .loginPage("/login")
                .and()
                .logout();
    }
}
