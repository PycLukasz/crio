package com.example.crio.profiles;

import com.example.crio.model.Patient;
import com.example.crio.properties.AppConfigProperties;
import com.example.crio.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class DevService {
    @Autowired
    private PatientRepository patientRepository;
    @Autowired
    private AppConfigProperties appConfigProperties;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @PostConstruct
    public void addDefaultPatient(){

        Patient patient = Patient.builder()
                .name(appConfigProperties.getName())
                .surname(appConfigProperties.getSurname())
                .login(appConfigProperties.getLogin())
                .password(passwordEncoder.encode(appConfigProperties.getPassword()))
                .isAdmin(appConfigProperties.isAdmin())
                .pesel(appConfigProperties.getPesel())
                .street(appConfigProperties.getStreet())
                .postalCode(appConfigProperties.getPostalCode())
                .city(appConfigProperties.getCity())
                .email(appConfigProperties.getEmail())
                .phoneNumber(appConfigProperties.getPhoneNumber())
                .age(appConfigProperties.getAge())
                .type(appConfigProperties.getType())
                .discipline(appConfigProperties.getDiscipline())
                .build();
        Patient addedPatient = patientRepository.save(patient);
    }

}
