package com.example.crio.profiles;

import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@Profile("dev")
@RestController
public class DevTestController {
    @GetMapping("testController")
    public String helloWorld(){
        return "Hello World from Dev profile!";
    }
}
