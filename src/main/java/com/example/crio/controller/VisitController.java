package com.example.crio.controller;

import com.example.crio.controller.exception.BadRequestException;
import com.example.crio.controller.exception.NotFoundException;
import com.example.crio.service.VisitService;
import com.example.crio.service.dto.ReturnVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.List;

@Controller
public class VisitController {
    @Autowired
    private VisitService visitService;

    @GetMapping("/getVisits")
    public List<ReturnVisitDTO> getTodaysVisits() throws NotFoundException {
        return visitService.visitsOfDay(LocalDate.now());
    }


    @GetMapping("/deleteVisit")
    public String deleteVisit(@RequestParam int id) throws BadRequestException {
        visitService.deleteVisit(id);
        return "redirect:/timetable";
    }

    @GetMapping("/visitToDelete")
    public String deleteVisit2(@RequestParam int id) throws BadRequestException {
        visitService.deleteVisit(id);
        return "redirect:/patientHistory";
    }
}
