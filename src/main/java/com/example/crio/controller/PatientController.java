package com.example.crio.controller;

import com.example.crio.service.dto.CreatePatientDTO;
import com.example.crio.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ap/v1/pacient")
public class PatientController {
    @Autowired
    private PatientService patientService;

    @PostMapping
    public CreatePatientDTO createNewPatient(@RequestBody CreatePatientDTO newPatient){
        return patientService.createNewPatient(newPatient);
    }


}
